var express = require('express'),
    app = express();

var CLIENT_ID =  "ENTER_YOUR_CLIENT_ID_HERE";
var CLIENT_SECRET = "ENTER_YOUR_CLIENT_SECRET_HERE";

var oauth2 = require('simple-oauth2')({
    clientID: CLIENT_ID,
    clientSecret: CLIENT_SECRET,
    site: 'https://bitbucket.org',
    tokenPath: '/site/oauth2/access_token',
    authorizationPath: '/site/oauth2/authorize'
});

// Authorization uri definition
var authorization_uri = oauth2.authCode.authorizeURL({
  redirect_uri: 'http://localhost:3000/callback'
  // scope: 'repository_write'
});

// Initial page redirecting to Github
app.get('/auth', function (req, res) {
    res.redirect(authorization_uri);
});

// Callback service parsing the authorization token and asking for the access token
app.get('/callback', function (req, res) {
  var code = req.query.code;
  console.log('/callback');
  oauth2.authCode.getToken({
    code: code,
    redirect_uri: 'http://localhost:3000/callback'
  }, saveToken);

  function saveToken(error, result) {
    if (error) {
        console.log('Access Token Error', error);
        res.send('ERROR <strong>' + (error.message ? error.message : error.error_description) + '</strong>');
        return;
    }
    token = oauth2.accessToken.create(result);

    console.log('Access Token Error', token);
    res.send('SUCCESS <pre>' + JSON.stringify(token, null, "\t") + '</pre>');
  }
});

app.get('/', function (req, res) {
  res.send('<a href="/auth">Authorise</a>');
});

app.listen(3000);

console.log('Express server started at http://localhost:3000/');
